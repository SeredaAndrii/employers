function Notify() {
    PNotify.prototype.options.styling = "fontawesome";

    this.success = function(text) {
        new PNotify({
            title: 'Success',
            text: text,
            type: 'success'
        });
    };

    this.error = function(text) {
        new PNotify({
            title: 'Error',
            text: text,
            type: 'error'
        });
    };

    this.info = function(text) {
        new PNotify({
            title: 'Info',
            text: text,
            type: 'info'
        });
    };

    this.confirm = function(text, onOKClick, onCancelClick) {
        (new PNotify({
            title: 'Confirmation',
            text: text,
            hide: false,
            confirm: { confirm: true },
            buttons: {
                closer: false,
                sticker: false
            },
            addclass: 'stack-popup',
            stack: {
                'dir1': 'down',
                'dir2': 'right',
                'modal': true
            },
            history: { history: false }
        })).get().
        on('pnotify.confirm', function() {
            $("div.ui-pnotify-popup-overlay").remove();
            if(onOKClick !== undefined)
            {
                onOKClick();
            }
        }).
        on('pnotify.cancel', function() {
            $("div.ui-pnotify-popup-overlay").remove();
            if(onCancelClick !== undefined)
            {
                onCancelClick();
            }
        });
    };

    this.prompt = function(text, defaultVal, onOKClick, onCancelClick) {
        (new PNotify({
            title: 'Confirmation',
            text: text,
            hide: false,
            confirm: {
                confirm: true,
                prompt: true,
                prompt_default: defaultVal
            },
            buttons: {
                closer: false,
                sticker: false
            },
            addclass: 'stack-popup',
            stack: {
                'dir1': 'down',
                'dir2': 'right',
                'modal': true
            },
            history: { history: false }
        })).get().
        on('pnotify.confirm', function(e, notice, value)
        {
            $("div.ui-pnotify-popup-overlay").remove();
            if(onOKClick !== undefined)
            {
                onOKClick(value);
            }
            return value;
        }).
        on('pnotify.cancel', function() {
            $("div.ui-pnotify-popup-overlay").remove();
            if(onCancelClick !== undefined)
            {
                onCancelClick();
            }
        });
    };
}
