function Employers()
{
    $('.remove').click(this.onRemoveClick.bind(this));

    setTimeout(function() {
        $('.alert').fadeOut('fast');
    }, 2000);
}

Employers.prototype.onRemoveClick = function(e)
{
    e.preventDefault();

    var row = $(e.target).closest('tr');
    var id = $(e.target).closest('td').data('id');

    notify.confirm('Are you sure want to delete current employee?', function()
    {
        $.post('/employee-remove', {id: id }, function(answer)
        {
            notify.success(answer.message);
            row.remove();
        })
        .fail(function(data)
        {
            notify.error(getMessageTextFromErrorResponse(data));
        });
    });
};


$(function()
{
    new Employers();
});