$(document).ready(function()
{
    notify = new Notify();
});

function getMessageTextFromErrorResponse(data)
{
    if(data.status === 403)
        return 'Access is denied';

    var errors = $.parseJSON(data.responseText);
    return arrayToList(errors);
}

function arrayToList(arr)
{
    var result = '';

    $.each(arr, function(index, value)
    {
        result += '<li>' + value + '</li>';
    });

    return '<ul>' + result + '</ul>';
}

jQuery.fn.setVisibility = function(visible) {
    return visible ? this.show() : this.hide();
};


