function Position()
{
    $('table').on('click', 'a.edit', this.showModal.bind(this));
    $('#addPosition').click(this.showModal.bind(this));
    $('#savePosition').click(this.onSaveClick.bind(this));
    $('.remove').click(this.onRemoveClick.bind(this));
    this.popup = $('#positionModal');

}

Position.prototype.showModal = function(e)
{
    e.preventDefault();

    var id = $(e.target).closest('td').data('id');
    var title = $(e.target).closest('td').data('title');
    var lastClassName = $(e.currentTarget).attr('class').split(' ').pop();

    this.popup.modal('show');
    this.changeModalTitle(lastClassName);

    $('#positionId').val(id);
    $('#positionTitle').val(title);
};

Position.prototype.changeModalTitle = function(title)
{
    title = title.charAt(0).toUpperCase() + title.slice(1);
    this.popup.find('h5 span').text(title);
};

Position.prototype.onSaveClick = function()
{
    var form = $('form[name=positionForm]');
    this.popup.modal('hide');

    $.post(form.attr('action'), form.serialize(), function(answer)
    {
        notify.success(answer.message);
        $('table').html(answer.view);
    })
    .fail(function(data)
    {
        notify.error(getMessageTextFromErrorResponse(data));
    });
};

Position.prototype.onRemoveClick = function(e)
{
    e.preventDefault();

    var row = $(e.target).closest('tr');
    var id = $(e.target).closest('td').data('id');

    notify.confirm('Are you sure want to delete current position?', function()
    {
        $.post('/position-remove', {id: id }, function(answer)
        {
            notify.success(answer.message);
            row.remove();
        })
        .fail(function(data)
        {
            notify.error(getMessageTextFromErrorResponse(data));
        });
    });
};


$(function()
{
    new Position();
});