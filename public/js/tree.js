function Tree()
{
    $('.dd').nestable({
        maxDepth: 10,
        callback: function (mainContainer, movedItem) {
            var o = this.options;
            var movedItemId = $(movedItem).attr('data-id');
            var parentId = $(movedItem).parent(o.listNodeName).parent(o.itemNodeName).attr('data-id');

            $.post('/employee-move', {employee_id: movedItemId, chef_id: parentId} )
            .fail(function(data)
            {
                notify.error(getMessageTextFromErrorResponse(data));
            });;
        }
    });
}

$(function()
{
    new Tree();
});