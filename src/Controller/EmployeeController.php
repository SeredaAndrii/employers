<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use function PHPSTORM_META\elementType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeController extends AbstractController
{
    /**
     * @Route("/employee/{id}", requirements={"id"="\d+"},  name="employee_show")
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function new(Request $request, int $id = null)
	{
		$repository = $this->getDoctrine()->getRepository(Employee::class);

		if(! is_null($id))
		{
			$employee = $repository->find($id);
			$form     = $this->createForm(EmployeeType::class, $employee);
			$title    = 'Edit '.$employee->getFullName();
		}
		else{
			$form  = $this->createForm(EmployeeType::class, null);
			$title = 'New Employee';
		}

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
             $employee = $form->getData();
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($employee);
             $entityManager->flush();

            $this->addFlash('success', 'Saved');
            return $this->redirectToRoute('employers_list');
        }

		return $this->render('forms/employee.html.twig', ['form' => $form->createView(), 'title' => $title]);
	}

	/**
	 * @Route("/", name="employers_list")
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function index()
	{
		$employers = $this->getDoctrine()->getRepository(Employee::class)->findAll();
		if($employers === null)
			return new Response('There are no employers exist');

		return $this->render('employers_table.html.twig', ['employers' => $employers]);

	}

    /**
     * @Route("/employee-remove", name="employee_remove")
     * @param Request $request
     * @return JsonResponse
     */
    public function remove(Request $request)
    {
        $employee = $this->getDoctrine()->getRepository(Employee::class)->find($request->get('id'));

        $em = $this->getDoctrine()->getManager();
        $em->remove($employee);
        $em->flush();

        return new JsonResponse(['message' => 'Employee '. $employee->getFullName(). ' was deleted']);

    }

    /**
     * @Route("/employee-tree", name="employee_tree")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function employersTree()
    {
        $em = $this->getDoctrine()->getRepository(Employee::class);
        $subordinates = $em->getAllSubordinates();

        return $this->render('employers_tree.html.twig', ['subordinates' => $subordinates]);

    }

    /**
     * @Route("/employee-move", name="employee_move")
     * @param Request $request
     * @return JsonResponse
     */
    public function moveEmployee(Request $request)
    {
        $em = $this->getDoctrine()->getRepository(Employee::class);
        $employeeId = $request->get('employee_id');

        if(!is_null($employeeId))
        {
            $employee = $em->find($employeeId);
            $chefId = $request->get('chef_id');

            if(is_null($chefId))
            {
                $employee->setChef();
            }
            else
            {
                $chef = $em->find($chefId);
                $employee->setChef($chef);
            }

            $this->getDoctrine()->getManager()->flush();

            return  new JsonResponse(['message' => 'ok']);
        }

        return  new JsonResponse(['message' => 'fail']);
    }

}