<?php

namespace App\Controller;

use App\Entity\Position;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PositionController extends AbstractController
{
	/**
	 * @Route("/positions", name="positions")
	 */
	public function index()
	{
		$positions = $this->getDoctrine()->getRepository(Position::class)->findAll();
		if($positions === null)
			return new Response('There are no positions exist');

		return $this->render('positions_table.html.twig', ['positions' => $positions]);
	}

	/**
	 * @Route("/position", name="position_save")
	 * @param Request $request
	 * @param ValidatorInterface $validator
	 * @return JsonResponse
	 */
	public function save(Request $request,ValidatorInterface $validator)
	{

		if($request->get('id'))
			$position = $this->getDoctrine()->getRepository(Position::class)->find($request->get('id'));
		else
			$position = new Position();

		$position->setTitle($request->get('title'));
		$errors = $validator->validate($position);

		if(count($errors) > 0)
			return new JsonResponse($this->getErrorsFromValidator($errors), 422);

		$em = $this->getDoctrine()->getManager();
		$em->merge($position);
		$em->flush();

		$positions = $this->getDoctrine()->getRepository(Position::class)->findAll();
		$view = $this->render('inc/position_table.html.twig', ['positions' =>$positions])->getContent();

		return new JsonResponse(['message' => 'Saved', 'view' => $view]);

	}


	/**
	 * @Route("/position-remove", name="position_remove")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function remove(Request $request)
	{
		$position = $this->getDoctrine()->getRepository(Position::class)->find($request->get('id'));

		$em = $this->getDoctrine()->getManager();
		$em->remove($position);
		$em->flush();

		return new JsonResponse(['message' => 'Position '. $position->getTitle(). ' was deleted']);

	}

	private function getErrorsFromValidator(ConstraintViolationListInterface $errors)
	{
		$formattedErrors = [];
		foreach ($errors as $error) {
			$formattedErrors[$error->getPropertyPath()] = $error->getMessage();
		}

		return $formattedErrors;
	}
}