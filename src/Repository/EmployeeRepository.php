<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Employee::class);
	}

	//    /**
	//     * @return Employee[] Returns an array of Employee objects
	//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('e.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Employee
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

//	public function findByNot(array $criteria, array $orderBy = null, $limit = null, $offset = null)
//	{
//		$qb   = $this->getEntityManager()->createQueryBuilder();
//		$expr = $this->getEntityManager()->getExpressionBuilder();
//
//		$qb->select('entity')->from($this->getEntityName(), 'entity');
//
//		foreach($criteria as $field => $value)
//		{
//
//			$qb->andWhere($expr->neq('entity.'.$field, $value));
//		}
//
//		if($orderBy)
//		{
//
//			foreach($orderBy as $field => $order){
//
//				$qb->addOrderBy('entity.'.$field, $order);
//			}
//		}
//
//		if($limit)
//			$qb->setMaxResults($limit);
//
//		if($offset)
//			$qb->setFirstResult($offset);
//
//		return $qb->getQuery()->getResult();
//	}

    public function getAllSubordinates(Employee $chef = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->orderBy('e.lastName', 'ASC');

        if (is_null($chef))
        {
            $qb->andWhere('e.chef IS NULL');
        } else
        {
            $qb->andWhere('e.chef = :chef')->setParameter('chef', $chef->getId());
        }

        $arr = $qb->getQuery()->getResult();
        $result = [];

        foreach ($arr as $subordinate)
        {
            $item = array(
                'employee'     => $subordinate,
                'children' => self::getAllSubordinates($subordinate),
            );
            $result[$subordinate->getId()] = $item;
        }

        return $result;
    }

}
