<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PositionRepository")
 * @UniqueEntity(fields={"title"})
 */
class Position
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @ORM\Column(type="string", length=50, unique=true)
	 * @Assert\NotBlank()
	 */
    private $title;

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title): void
	{
		$this->title = $title;
	}

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Employee", mappedBy="position")
	 * @ORM\JoinColumn(nullable=true, onDelete="cascade")
	 */
	private $employers;

	/**
	 * @return Employee
	 */
	public function getEmployers()
	{
		return $this->employers;
	}

}
