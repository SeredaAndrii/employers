<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @ORM\Column(type="string", length=50)
	 * @Assert\NotBlank()
	 * @Assert\Length(min="2", max="20")
	 */
	private $firstName;

	/**
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}


	/**
	 * @ORM\Column(type="string", length=50)
	 * @Assert\NotBlank()
	 * @Assert\Length(min="2", max="20")
	 */
	private $lastName;

	/**
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName($firstName): void
	{
		$this->firstName = $firstName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName): void
	{
		$this->lastName = $lastName;
	}

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Position", inversedBy="employers")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $position;

	/**
	 * @return Position
	 * @Assert\NotBlank()
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position): void
	{
		$this->position = $position;
	}

	/**
	 * @ORM\Column(type="integer", length=25)
	 * @Assert\Length(min="5", max="25")
	 * @Assert\Type("integer")
	 */
	private $phone;

	/**
	 * @return integer
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone($phone): void
	{
		$this->phone = $phone;
	}

	/**
	 * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.")
     */
	private $email;

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email): void
	{
		$this->email = $email;
	}

	/**
	 * @ORM\Column(type="string", length=150, nullable=true)
	 */
	private $notes;

	/**
	 * @return string
	 */
	public function getNotes()
	{
		return $this->notes;
	}

	/**
	 * @param mixed $notes
	 */
	public function setNotes($notes): void
	{
		$this->notes = $notes;
	}

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="subordinates")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $chef;

	/**
	 * @return self
	 */
	public function getChef()
	{
		return $this->chef;
	}

	/**
	 * @param self $chef
	 */
	public function setChef(Employee $chef = null): void
	{
        if($this->getChef() !== $chef)
        {
            $this->chef = $chef;
        }
	}

	public function hasChef()
	{
		return $this->chef !== null;
	}

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Employee", mappedBy="chef")
	 */
	private $subordinates;

	/**
	 * @return string
	 */

	public function getFullName()
	{
		return $this->getFirstName().' '.$this->getLastName();
	}

	public function __construct()
	{
		$this->subordinates = new ArrayCollection();
	}

	/**
	 * @return Collection
	 */
	public function getSubordinates()
	{
		return $this->subordinates;
	}

	/**
	 * @param Employee $subordinates
	 * @return $this
	 */
	public function addSubordinate (Employee $subordinates)
	{
		$this->subordinates[] = $subordinates;

		return $this;
	}

}
