<?php
namespace App\Form;

use App\Entity\Employee;
use App\Entity\Position;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
    {

	    $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('position', EntityType::class, ['class' => Position::class, 'choice_label' => 'title'])
            ->add('phone', IntegerType::class)
            ->add('email', EmailType::class)
            ->add('notes', TextareaType::class, ['required' => false])
            ->add('save', SubmitType::class);
//	        ->add('chef', EntityType::class,
//		        ['class' => Employee::class,
//			        'label' => 'Subordinates',
//			        'multiple' => true,
//			        'choice_label' => function(Employee $employee){ return $employee->getFullName(); },
//			        'query_builder' => function(EntityRepository $er) use ($id)
//			        {
//			        	if($id)
//				            return $er->createQueryBuilder('e')->where('e.chef = :id')->setParameter('id', $id);
//			        	else
//				            return $er->createQueryBuilder('e')->where('e.chef is NULL');
//			        },
//		        ]
//	        );
    }

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Employee::class,
		));
	}
}